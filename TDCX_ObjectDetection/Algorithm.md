# SingleShotDetector Resnet50

In this project we will be implemnting one of the fastest object detection algorithm ![Single Shot Detector](https://arxiv.org/pdf/1512.02325.pdf) with Resnet50 as backbone

![](https://miro.medium.com/max/1200/1*ikCbS_gUErnx9Gda7gZ7Ng.png)

I have taken a pretrained model from Tensorflow model Zoo pretrained on COCO dataset since it has vide varity of datasets so it will be able to converge fast.

Single Shot detector like YOLO takes only one shot to detect multiple objects present in an image using multibox.High speed and accuracy of SSD using relatively low resolution images is attributed due to following reasons

    1. Eliminates bounding box proposals like the ones used in RCNN’s
    2. Includes a progressively decreasing convolutional filter for predicting object categories and offsets in bounding box locations.

High detection accuracy in SSD is achieved by using multiple boxes or filters with different sizes, and aspect ratio for object detection. It also applies these filters to multiple feature maps from the later stages of a network. This helps perform detection at multiple scales.

#### Hyper parameters used

Key details - 

    num_classes: 3 # Number of Classes to classify
    batch_size: 8 #due to less GPU memry
    num_steps: 25000 #but trained till 14300 due to lack of free online GPU time
    
please reffer to ![ pipeline.config](https://gitlab.com/chekoduadarsh/tdcx_bao_detector/-/blob/master/TDCX_ObjectDetection/workspace/training/models/my_ssd_resnet50_v1_fpn/pipeline.config) for further details