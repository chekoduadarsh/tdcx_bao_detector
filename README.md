# Banana Apple Orange Object detector

<span style="color:blue">*A Tensorflow Object Detection React App Served with Flask*</span>.

### Task:

```
Creat an React app which detectd Banana, Apple, Orange using Tensorflow in backend
```

## Code Architecture

![](https://i.ibb.co/PwBMDs8/Untitled-Diagram.png)

## Algorithm

we used SDD with Resnet50 in our backed as object detector for further config please have alook into ![`TDCX_ObjectDetection/Algorithm.md](https://gitlab.com/chekoduadarsh/tdcx_bao_detector/-/blob/master/TDCX_ObjectDetection/Algorithm.md) 

**Folder `TDCX_ObjectDetection` contains codes to Train the model wither use the given colab notebook to train the model in the google colab (tested/used  due to lack of gpu support in my local machine) or else follow the commands below.**

before starting to train the model downlaod and unzip ![ssd_resnet50_v1_fpn_640x640_coco17_tpu-8](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md) from tensorflow zoo and keep it in `/TDCX_ObjectDetection/workspace/training/pre-trained-models`. Also keep traing and testing images with the xml annotation files at `/TDCX_ObjectDetection/workspace/training/images`


```
cd pathTORepo+'/TDCX_ObjectDetection/models/research/'
protoc object_detection/protos/*.proto --python_out=.


cp object_detection/packages/tf2/setup.py .
python -m pip install . 


python object_detection/builders/model_builder_tf2_test.py

pip install opencv-python
pip install opencv-contrib-python


cd pathTORepo+"/TDCX_ObjectDetection/workspace/training"

python model_main_tf2.py --model_dir=models/my_ssd_resnet50_v1_fpn --pipeline_config_path=models/my_ssd_resnet50_v1_fpn/pipeline.config


python exporter_main_v2.py --input_type image_tensor --pipeline_config_path ./models/my_ssd_resnet50_v1_fpn/pipeline.config --trained_checkpoint_dir ./models/my_ssd_resnet50_v1_fpn/ --output_directory ./exported-models/my_model

```

**Folder `TDCX_ObjectDetection_App` contains react app use the following steps run the App.**

```
cd pathTORepo+'/TDCX_ObjectDetection_App/flask-backend/'
conda install --file requirement.txt

cd pathTORepo+'/TDCX_ObjectDetection_App/react-frontend/'
npm install

```

note : Make sure that you have Object deteciton API from tensorflow is configured properly else please do the following commands
```

cd pathTORepo+'/TDCX_ObjectDetection/models/research/'
protoc object_detection/protos/*.proto --python_out=.


cp object_detection/packages/tf2/setup.py .
python -m pip install . 

```

## Demo Video


[![Demo](https://img.youtube.com/vi/gNE2GRk8_3I/0.jpg)](https://www.youtube.com/watch?v=gNE2GRk8_3I)

*sorry for the low fps due to my laptops limited processing capabilities*


# tested and implemented -> Ubuntu 20.04