
import tensorflow as tf
import time
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as viz_utils

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import warnings



def load_image_into_numpy_array(file):
    return np.array(file)

def FasterRCNN(image):
    img = [image]
    warnings.filterwarnings('ignore')   # Suppress Matplotlib warnings

    PATH_TO_SAVED_MODEL = "Object_detectors/exported-models/my_model/saved_model"
    detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL)
    category_index = label_map_util.create_category_index_from_labelmap("Object_detectors/annotations/label_map.pbtxt",
                                                                        use_display_name=True)
    for image_path in img:
        image_np=load_image_into_numpy_array(image_path)
        input_tensor=tf.convert_to_tensor(image_np)
        input_tensor=input_tensor[tf.newaxis, ...]

        detections=detect_fn(input_tensor)

        num_detections=int(detections.pop('num_detections'))
        detections={key:value[0,:num_detections].numpy()
                       for key,value in detections.items()}
        detections['num_detections']=num_detections

        # detection_classes should be ints.
        detections['detection_classes']=detections['detection_classes'].astype(np.int64)

        image_np_with_detections=image_np.copy()

        viz_utils.visualize_boxes_and_labels_on_image_array(
              image_np_with_detections,
              detections['detection_boxes'],
              detections['detection_classes'],
              detections['detection_scores'],
              category_index,
              use_normalized_coordinates=True,
              max_boxes_to_draw=10,     #max number of bounding boxes in the image
              min_score_thresh=.7,      #min prediction threshold
              agnostic_mode=False)

        return image_np_with_detections