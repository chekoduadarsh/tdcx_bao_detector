import cv2
import flask
import numpy as np
from PIL import Image
from io import BytesIO
import base64
from flask import request
from Object_detectors.Faster_RCNN_Inference import FasterRCNN

app =flask.Flask("__main__")

def pil_image_to_base64(pil_image):
    buf = BytesIO()
    pil_image.save(buf, format="JPEG")
    return base64.b64encode(buf.getvalue())


def base64_to_pil_image(base64_img):
    return Image.open(BytesIO(base64.b64decode(base64_img)))


@app.route("/")
def my_index():
    return flask.render_template("index.html")


@app.route('/ObjectDetection', methods=['POST','GET'])
def DFRreturn():

    inputImagebase64 = list(request.form.to_dict().keys())[0].replace(" ", "+")+"===="
    print("")
    print(bytes(inputImagebase64.replace("data:image/jpeg;base64,",""), 'utf-8'))
    inputImagebase64 = bytes(inputImagebase64.replace("data:image/jpeg;base64,",""), 'utf-8')
    print("")


    input_img = base64_to_pil_image(inputImagebase64)
    open_cv_image = np.array(input_img)
    print("done reading")
    output_img = FasterRCNN(input_img)
    output_str = pil_image_to_base64(Image.fromarray(output_img))
    output_str = output_str.decode("utf-8")
    output_str = "data:image/jpeg;base64," + output_str
    print("done predicting")
    return output_str


app.run(debug=True)